#include "ofApp.h"


//--------------------------------------------------------------
vector<FoundSquare> ofApp::loadArchive() {
    string archivePath = ofToDataPath("archive");
    ofDirectory archiveDir(archivePath);
    archiveDir.listDir();
    
    vector<FoundSquare> archivedSamples(archiveDir.size());
    
    for (int i = 0; i < archiveDir.size(); i++) {
        string samplePath = archiveDir.getPath(i);
        
        // Load the XML file and save to a buffer
        ofFile file;
        file.open(samplePath + "/sampleSettings.xml");
        ofBuffer buffer = file.readToBuffer();
        
        // Create an XML object and set it with the buffer
        ofXml xml;
        xml.loadFromBuffer(buffer.getText());
        xml.setTo("Settings");
        
        // Create a new FoundSquare and set its members with the XML
        FoundSquare fs;
        fs.img.load(samplePath + "/img.png");
        fs.label = xml.getValue("label");
        fs.trainingLabel = xml.getIntValue("trainingLabel");
        fs.isPrediction = xml.getBoolValue("isPrediciton");
        fs.isArchived = xml.getBoolValue("isArchived");
        fs.rect = cv::Rect(); // TODO save and load the right data
        fs.area = xml.getFloatValue("area");
        
        // Add the FoundSquare to the vector.
        archivedSamples.at(i) = fs;
    }
    
    return archivedSamples;
}

//--------------------------------------------------------------
int ofApp::getCameraID(ofVideoGrabber grabber, string name) {
    vector<ofVideoDevice> devices = grabber.listDevices();
    
    for (vector<ofVideoDevice>::iterator it = devices.begin(); it != devices.end(); ++it) {
        if (it->deviceName == name) {
            ofLog(OF_LOG_NOTICE, "Using Camera: " + name);
            return it->id;
            break;
        }
    }
    
    ofLog(OF_LOG_ERROR, "Could not find the requested camera...");
    
    if (devices.size() > 0) {
        ofLog(OF_LOG_ERROR, "Using default camera: " + devices.at(0).deviceName);
        return 0;
    }else {
        ofLog(OF_LOG_ERROR, "No camera attached...");
        ofExit();
        return -1;
    }
}

//--------------------------------------------------------------
void FoundSquare::draw() {
    img.draw(0, 0);
    string labelStr = "no class";
    
    labelStr = (isPrediction ? "predicted: " : "assigned: ") + label;
    
    ofDrawBitmapStringHighlight(labelStr, 4, img.getHeight()-22);
    
    ofDrawBitmapStringHighlight("{"+ofToString(rect.x)+","+ofToString(rect.y)+","+ofToString(rect.width)+","+ofToString(rect.height)+"}, area="+ofToString(area), 4, img.getHeight()-5);
}

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetWindowShape(1600, 900);
    
    width = 640;
    height = 480;
    
#ifdef RELEASE
    ccv.setup(ofToDataPath("image-net-2012.sqlite3"));
#else
    ccv.setup(ofToDataPath("../../../../data/image-net-2012.sqlite3"));
#endif
    
    bAdd.addListener(this, &ofApp::addSamplesToTrainingSetNext);
    bTrain.addListener(this, &ofApp::trainClassifier);
    bClassify.addListener(this, &ofApp::classifyNext);
    bSave.addListener(this, &ofApp::save);
    bLoad.addListener(this, &ofApp::load);
    trainingLabel.addListener(this, &ofApp::setTrainingLabel);
    
    
    // Get class names from XML file
    ofXml xml;
    xml.load(ofToDataPath("settings.xml"));
    xml.setTo("DoodleSynthSettings");
    xml.setTo("CameraName");
    
    cam.setDeviceID(ofApp::getCameraID(cam, xml.getValue()));
    cam.setup(width, height);
    
    xml.setToParent();
    
    if (xml.exists("classes") && xml.setTo("classes") && xml.exists("class[0]")) {
        xml.setTo("class[0]");
        classNames.clear();
        
        do {
            string newClass = xml.getValue();
            classNames.push_back(newClass);
        }
        while(xml.setToSibling());
    }
    
    gui.setup();
    gui.setName("DoodleSynth");
    ofParameterGroup gCv;
    gCv.setName("CV initial");
    gCv.add(minArea.set("Min area", 10, 1, 100));
    gCv.add(maxArea.set("Max area", 200, 1, 500));
    gCv.add(threshold.set("Threshold", 128, 0, 255));
    gCv.add(nDilate.set("Dilations", 1, 0, 8));
    gui.add(trainingLabel.set("Training Label", 0, 0, classNames.size()-1));
    gui.add(bAdd.setup("Add samples"));
    gui.add(bTrain.setup("Train"));
    gui.add(bRunning.setup("Run", false));
    gui.add(bClassify.setup("Classify"));
    gui.add(bSave.setup("Save"));
    gui.add(bLoad.setup("Load"));
    gui.add(gCv);
    gui.setPosition(0, 400);
    gui.loadFromFile("settings_cv.xml");
    
    fbo.allocate(width, height);
    colorImage.allocate(width, height);
    grayImage.allocate(width, height);
    
    isTrained = false;
    toAddSamples = false;
    toClassify = false;
    
    trainingData.setNumDimensions(4096);
    AdaBoost adaboost;
    adaboost.enableNullRejection(false);
    adaboost.setNullRejectionCoeff(3);
    pipeline.setClassifier(adaboost);
    
    addSamplesToTrainingSet();
    trainClassifier();
    
    for (int i = 0; i < 4; i++) {
        loopA[i].load("audio/a" + ofToString(i) + ".wav");
        loopA[i].setLoop(true);
        loopA[i].setVolume(MUTE);
        loopA[i].play();
    }
}

//--------------------------------------------------------------
void ofApp::update(){
    cam.update();
    
    if(cam.isFrameNew()) {
        // get grayscale image and threshold
        colorImage.setFromPixels(cam.getPixels());
        grayImage.setFromColorImage(colorImage);
        
        for (int i = 0; i < nDilate; i++) {
            grayImage.erode_3x3();
        }
        
        grayImage.threshold(threshold);
        
        // find initial contours
        contourFinder.setMinAreaRadius(minArea);
        contourFinder.setMaxAreaRadius(maxArea);
        contourFinder.setThreshold(127);
        contourFinder.findContours(grayImage);
        contourFinder.setFindHoles(true);
        
        // draw all contour bounding boxes to FBO
        fbo.begin();
        ofClear(ofColor::black);
        ofFill();
        ofSetColor(ofColor::white);
        
        for (int i = 0; i < contourFinder.size(); i++) {
            ofBeginShape();
            
            for (auto p : contourFinder.getContour(i)) {
                ofVertex(p.x, p.y);
            }
            
            ofEndShape();
        }
        
        fbo.end();
        ofPixels pixels;
        fbo.readToPixels(pixels);
        
        // find merged contours
        contourFinder2.setMinAreaRadius(minArea);
        contourFinder2.setMaxAreaRadius(maxArea);
        contourFinder2.setThreshold(127);
        contourFinder2.findContours(pixels);
        contourFinder2.setFindHoles(true);
        
        if (toAddSamples) {
            addSamplesToTrainingSet();
            toAddSamples = false;
        }
        else if (isTrained && (bRunning || toClassify)) {
            classifyCurrentSamples();
            toClassify = false;
        }
    }
    
}

//--------------------------------------------------------------
void ofApp::draw() {
    ofBackground(ofColor::darkGray);
    
    ofPushMatrix();
    ofScale(0.75, 0.75);
    
    // original
    ofPushMatrix();
    ofPushStyle();
    ofTranslate(0, 20);
    cam.draw(0, 0);
    ofDrawBitmapStringHighlight("original", 0, 0);
    ofPopMatrix();
    ofPopStyle();
    
    // thresholded
    ofPushMatrix();
    ofPushStyle();
    ofTranslate(width, 20);
    grayImage.draw(0, 0);
    ofSetColor(ofColor::green);
    contourFinder.draw();
    ofDrawBitmapStringHighlight("thresholded", 0, 0);
    ofPopMatrix();
    ofPopStyle();
    
    // merged
    ofPushMatrix();
    ofPushStyle();
    ofTranslate(2 * width, 20);
    fbo.draw(0, 0);
    ofSetColor(ofColor::green);
    contourFinder2.draw();
    ofDrawBitmapStringHighlight("merged", 0, 0);
    ofPopMatrix();
    ofPopStyle();
    
    ofPopMatrix();
    
    // draw tiles
    ofPushMatrix();
    ofPushStyle();
    ofTranslate(210, 0.75 * height + 25);
    int nPerRow = max(5, (int) ceil(foundSquares.size()/2.0));
    ofTranslate(-ofMap(ofGetMouseX(), 0, ofGetWidth(), 0, max(0,nPerRow-5)*226), 0);
    
    for (int i = 0; i < foundSquares.size(); i++) {
        ofPushMatrix();
        ofTranslate(226*(i%nPerRow), 240*floor(i/nPerRow));
        foundSquares[i].draw();
        ofPopMatrix();
    }
    
    ofPopMatrix();
    ofPopStyle();
    
    gui.draw();
}

//--------------------------------------------------------------
void ofApp::archiveSamples(vector<FoundSquare> samples) {
    string archivePath = ofToDataPath("archive");
    ofDirectory archive (archivePath);
    archive.listDir();
    
    for (vector<FoundSquare>::iterator sample = samples.begin(); sample != samples.end(); sample++) {
        // Check to see if the sample is from the archive to avoid duplicates.
        if (sample->isArchived == false) {
            sample->isArchived = true;
            
            // Make a new directory in archive with unique name
            string newDirectoryPath = ofToDataPath("archive/" + std::to_string(archive.size()));
            string newImagePath = newDirectoryPath + "/img.png";
            string xmlPath = newDirectoryPath + "/sampleSettings.xml";
            
            archive.createDirectory(newDirectoryPath);
            
            // save sample image to directory
            sample->img.save(newImagePath);
            
            // create an xml file in directory
            ofXml xml;
            xml.addChild("Settings");
            xml.addValue("label", sample->label);
            xml.addValue("trainingLabel", sample->trainingLabel);
            xml.addValue("isPrediction", sample->isPrediction);
            xml.addValue("isArchived", sample->isArchived);
            xml.addValue("area", sample->area);
            
            // save data to xml file
            xml.save(xmlPath);
            
            // Update the number of files in the archive directory
            archive.listDir();
        }
    }
}

//--------------------------------------------------------------
void ofApp::exit() {
    gui.saveToFile(ofToDataPath("settings_cv.xml"));
    archiveSamples(foundSquares);
}

//--------------------------------------------------------------
void ofApp::gatherFoundSquares() {
    foundSquares.clear();
    
    for (int i = 0; i < contourFinder2.size(); i++) {
        FoundSquare fs;
        fs.trainingLabel = trainingLabel;
        fs.rect = contourFinder2.getBoundingRect(i);
        fs.area = contourFinder2.getContourArea(i);
        fs.img.setFromPixels(cam.getPixels());
        fs.img.crop(fs.rect.x, fs.rect.y, fs.rect.width, fs.rect.height);
        fs.img.resize(224, 224);
        foundSquares.push_back(fs);
    }
}

//--------------------------------------------------------------
void ofApp::addSamplesToTrainingSet() {
    ofLog(OF_LOG_NOTICE, "Adding samples...");
    gatherFoundSquares();
    
    for (int i = 0; i < foundSquares.size(); i++) {
        foundSquares[i].label = classNames[trainingLabel];
        vector<float> encoding = ccv.encode(foundSquares[i].img, ccv.numLayers()-1);
        VectorFloat inputVector(encoding.size());
        
        for (int i = 0; i < encoding.size(); i++) {
            inputVector[i] = encoding[i];
        }
        
        trainingData.addSample(trainingLabel, inputVector);
        ofLog(OF_LOG_NOTICE, " Added sample #"+ofToString(i)+" label="+ofToString(trainingLabel));
    }
    
    archiveSamples(foundSquares);
    
    // Add the archived samples from past sessions to the training set.
    // TODO: May be doubling archived samples in training set. Need to look into it.
    if (ofGetFrameNum() == 0) {
        ofLog() << "Loading archived images to training set...";
        
        vector<FoundSquare> archivedSamples = loadArchive();
        
        for (int i = 0; i < archivedSamples.size(); i++) {
            vector<float> encoding = ccv.encode(archivedSamples[i].img, ccv.numLayers()-1);
            VectorFloat inputVector(encoding.size());
            
            for (int i = 0; i < encoding.size(); i++) {
                inputVector[i] = encoding[i];
            }
            
            trainingData.addSample(archivedSamples[i].trainingLabel, inputVector);
            ofLog(OF_LOG_NOTICE, " Added sample #"+ofToString(i)+" label="+ofToString(archivedSamples[i].trainingLabel));
        }
    }
    
}

//--------------------------------------------------------------
void ofApp::trainClassifier() {
    ofLog(OF_LOG_NOTICE, "Training...");
    
    if (pipeline.train(trainingData)) {
        ofLog(OF_LOG_NOTICE, "getNumClasses: "+ofToString(pipeline.getNumClasses()));
    }
    
    isTrained = true;
    ofLog(OF_LOG_NOTICE, "Done training...");
}

//--------------------------------------------------------------
void ofApp::classifyCurrentSamples() {
    ofLog(OF_LOG_NOTICE, "Classifiying on frame "+ofToString(ofGetFrameNum()));
    gatherFoundSquares();
    
    Boolean loopAStatus[4];
    
    // Using a separate array to determine if the tracks should be muted or not.
    for (int i = 0; i < 4; i++) {
        loopAStatus[i] = false;
    }
    
    for (int i = 0; i < foundSquares.size(); i++) {
        vector<float> encoding = ccv.encode(foundSquares[i].img, ccv.numLayers()-1);
        VectorFloat inputVector(encoding.size());
        
        for (int i = 0; i < encoding.size(); i++) {
            inputVector[i] = encoding[i];
        }
        
        if (pipeline.predict(inputVector)) {
            // gt classification
            int label = pipeline.getPredictedClassLabel();
            foundSquares[i].isPrediction = true;
            foundSquares[i].label = classNames[label];
            loopAStatus[label] = true; // Unmutes tracks for found shapes
        }
    }
    
    // Set the track volume accoriding to the loop status array
    for (int i = 0; i < 4; i++) {
        loopA[i].setVolume(loopAStatus[i] ? FULL : MUTE);
    }
}


//--------------------------------------------------------------
void ofApp::setTrainingLabel(int & label_) {
    trainingLabel.setName(classNames[label_]);
}

//--------------------------------------------------------------
void ofApp::save() {
    pipeline.save(ofToDataPath("doodleclassifier_model.grt"));
    archiveSamples(foundSquares);
}

//--------------------------------------------------------------
void ofApp::load() {
    pipeline.load(ofToDataPath("doodleclassifier_model.grt"));
    isTrained = true;
}

//--------------------------------------------------------------
void ofApp::classifyNext() {
    toClassify = true;
}

//--------------------------------------------------------------
void ofApp::addSamplesToTrainingSetNext() {
    toAddSamples = true;
}
