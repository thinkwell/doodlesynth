#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxCv.h"
#include "ofxGui.h"
#include "ofxGrt.h"
#include "ofxCcv.h"

using namespace ofxCv;
using namespace cv;

struct FoundSquare {
    ofImage img;
    string label;
    int trainingLabel;
    bool isPrediction = false;
    bool isArchived = false;
    cv::Rect rect;
    float area;
    void draw();
};


class ofApp : public ofBaseApp {
    public:

    // default class names if none found in settings.xml
    vector<string> classNames = {
        "shapeA",
        "shapeB",
        "shapeC",
        "shapeD",
    };

    void setup();
    void update();
    void draw();
    void exit();
    
    int getCameraID(ofVideoGrabber grabber, String name);
    vector <FoundSquare> loadArchive();
    void setTrainingLabel(int & label_);
    void addSamplesToTrainingSet();
    void gatherFoundSquares();
    void trainClassifier();
    void classifyCurrentSamples();
    
    void addSamplesToTrainingSetNext();
    void classifyNext();
    void archiveSamples(vector<FoundSquare> samples);
    
    void save();
    void load();
    
    int width, height;
    
    const float FULL = 1.0;
    const float MUTE = 0.0;
    
    ofSoundPlayer loopA[4];
    
    ofVideoGrabber cam;
    ContourFinder contourFinder, contourFinder2;
    ofFbo fbo;
    ofxCvGrayscaleImage grayImage;
    ofxCvColorImage colorImage;
    
    ofxPanel gui;
    ofxToggle bRunning;
    ofxButton bAdd, bTrain, bClassify, bSave, bLoad;
    ofParameter<float> minArea, maxArea, threshold;
    ofParameter<int> nDilate;
    ofParameter<int> trainingLabel;
    
    vector<FoundSquare> foundSquares;
    
    ClassificationData trainingData;
    GestureRecognitionPipeline pipeline;
    ofxCcv ccv;
    bool isTrained, toAddSamples, toClassify;
};
