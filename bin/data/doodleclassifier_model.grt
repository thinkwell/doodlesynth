GRT_PIPELINE_FILE_V3.0
PipelineMode: CLASSIFICATION_MODE
NumPreprocessingModules: 0
NumFeatureExtractionModules: 0
NumPostprocessingModules: 0
Trained: 0
Info: 
PreProcessingModuleDatatypes:
FeatureExtractionModuleDatatypes:
ClassificationModuleDatatype:	AdaBoost
PostProcessingModuleDatatypes:
GRT_ADABOOST_MODEL_FILE_V2.0
Trained: 0
UseScaling: 0
NumInputDimensions: 0
NumOutputDimensions: 0
NumTrainingIterationsToConverge: 0
MinNumEpochs: 0
MaxNumEpochs: 100
ValidationSetSize: 20
LearningRate: 0.1
MinChange: 1e-05
UseValidationSet: 0
RandomiseTrainingOrder: 1
UseNullRejection: 0
ClassifierMode: 0
NullRejectionCoeff: 3
PredictionMethod: 1
