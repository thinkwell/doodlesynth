## DoodleSynth

This application requires the following addons:  
- [ofxCv](https://github.com/kylemcdonald/ofxCv) 
- [ofxCcv](https://github.com/kylemcdonald/ofxCcv) 
- [ofxGrt](https://github.com/nickgillian/ofxGrt)

An interface for classifying drawings and objects. The app lets you define a series of object categories, examples drawn or shown under a webcam, and then learns to classify new objects. Doodle Synth saves learned samples in `data/archive` to get a larger learning set over time. 

The settings file `settings.xml` contains settings which you may wish to modify.  You may edit or add to the defined classes inside the `<classes>` tag. 

In the `data/audio` directory are four sound stems that play in a loop with each other. Each one of the four classified objects has a stem associated with it and will unmute the stem if it is recognized by the webcamera. If you want to change out the stems, make sure that you keep the audio file names the same.

Setting Up A Project...
::TODO::

To train your own objects/drawings...

1)  Delete all files and directories in the `data/archive` directory. Do not delete the actual `archive` directory.

2)  Run the program. 

3)  Select the desired identifier by the slider.

4)  Present the camera with the object that you want to train.

5)  Select the box `Add Sample`. A directory for the sample will be created in the archive directory. In the directory, an image of the object will be saved with an XML file that is used to classify the object in the program. Do not edit these manually.

6)  Reposition the object or if you're classifing a drawing, you can do many sample drawings at once. As long as they are the same classification, draw several of the same drawing and add them as normal. You can skip step 7 if you do several at once. 

7)  Repeat steps 4-6 several times. 

8)  Repeat steps 3-7 until all 4 objects are trained.

9)  Click Train to send the samples to the classifier.

10) Either click `Run` to classify objects continuously or `Classify` to classify objects from a snapshot.
